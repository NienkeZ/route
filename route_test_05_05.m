%%nageboodste lidar data als matrix wordt in dit script omgezet tot route
%%versie 1
clear;
close all;
clc;

tic
%% INPUT
bk=200;%breedte van de kar
cpd = 300; % control point afstand
avd = 400; %direct avoid distance
T = [-1000 1000 0 0 0 0]; %target: x, y, z, phi, r, alfa. phi = orientatie vanaf sensor, alfa is eigen rotatie. 
Txy = [T(1) T(2)]; %later afhankelijk van Trp
K = [0 0]; % x en y, vast op 0punt. afhankelijk van def 0punt en plaatsing lidar op kar
% bepaal punt p3 afhankelijk van alfaT
%% LiDAR data. 

d2r=2*pi/360;
LIr = readmatrix('data/Lidar_excel_4.xlsx','Range','E2:E637');% in mm?0
LIp = readmatrix('data/Lidar_excel_4.xlsx','Range','C2:C637')*d2r; %in rad
%% Lidar data omschrijven en filteren
LIx = LIr.*cos(LIp); 
LIy = LIr.*sin(LIp);
LI_0 = [LIx LIy];
idx2 = abs(LIy) <eps ;%| abs(LIr)>1000;
LI = [LIx(~idx2) LIy(~idx2)]; %nul data eruit halen. anders botst dit meteen
%LI is dus kleiner dan LIx en LIy
toc

%% obstakels clusteren
[idx, corepts] = dbscan(LI, bk, 5);
%% 3. Bezier
%f= figure(5);
f = figure('name','obstakels','numbertitle','on');
%scatter (LI(:, 1), LI(:, 2), 'x')
gscatter(LI(:,1), LI(:, 2), idx);
hold on; 
% scatter (LI_0(:, 1), LI_0(:, 2), 'd') % om te testen of er onterecht
% punten weggehaald worden. 



%run matrix:
M =[K(1,1) K(1,2);K(1,1) (K(1,2)+cpd);Txy(1,1) (Txy(1,2)-cpd);T(1) T(2)];%3e punt is overlappend aan 2e om matrixmaat constant te houden.

M(3,1) = cpd*sin(T(6))+T(1);
M(3,2) =-cpd*cos(T(6))+T(2);

P = B_Curve(M, LI);


hold on;
axis([-2000 2000 -2000 2000]);
grid on;




%% polybuffer met de route P? WERKT
polyout2 = polybuffer(P,'lines',bk/2,'JointType','miter');



p=M;    
n=size(M,1);
n1=n-1;
    for i=0:1:n1
    sigma(i+1)=factorial(n1)/(factorial(i)*factorial(n1-i));  % for calculating (x!/(y!(x-y)!)) values 
    end
    l=[];
    UB=[];
    maat = size(LI);
    for u=0:1/maat(1,1):1
    for d=1:n
    UB(d)=sigma(d)*((1-u)^(n-d))*(u^(d-1));
    end
    l=cat(1,l,UB);   
    end
Curve=l*p;



route_test_2 = inpolygon(LI(:,1), LI(:,2), polyout2.Vertices(:,1), polyout2.Vertices(:,2))';
raakt = LI( route_test_2==1,:);
scatter (raakt(:,1), raakt(:,2), 50)

%% ontwijkmaneuvra A: obstakels defineren. 
nr_of_groups = max(idx);
test_samen_m = [route_test_2' idx];
B_Plot(P, polyout2);
if any(route_test_2 ==1) %situatie: maar 1 groep staat in de weg
    group=0;
    
        

for group=1:1:nr_of_groups;
    test_samen_m(:,(group+2)) =  idx==group; %route_test_2'==1 &
    if any(test_samen_m(:, group+2) ==1 & route_test_2'==1) % er is een obstakel in deze groep. zoek linker en rechter data.  
            obst_r(group,1) = LI(find(test_samen_m(:, group+2),1,'first'),1); 
            obst_r(group,2) = LI(find(test_samen_m(:, group+2) ,1,'first'),2);%kan dit korter?
            obst_l(group,1) = LI(find(test_samen_m(:, group+2) ,1,'last'),1); 
            obst_l(group,2) = LI(find(test_samen_m(:, group+2) ,1,'last'),2); 
            obst = group;
             
    else 
            obst_r(group,:) = [0,0]; %aanvullen met nullen. 
            obst_l(group,:) = [0,0];
    end
end  
%nu weet de code waar obstakels zitten. 
%% controleer of linksom of rechtsom sneller is:
        polyout3 = polybuffer(P,'lines',40,'JointType','miter'); %20 is gegokt, tunen, afh van nr nuttige data.
        route_test_3 = inpolygon(LI(:,1), LI(:,2), polyout3.Vertices(:,1), polyout3.Vertices(:,2))';
        raakpunt = LI(route_test_3==1,:);
        [afst_P, loc_P] = min(sqrt((P(:,1)-raakpunt(1,1)).^2+(P(:,2)-raakpunt(1,2)).^2)); %geeft afstand en locatie van 1 punt van P het dichts bij het raakpunt. 
        alfa_P = atan(abs((P(loc_P+5,1)-P(loc_P,1)))/abs((P(loc_P+5,2)-P(loc_P,2))));
%factor_x = Richting_kiezen(P, loc_P, obst_l, obst_r, obst);  
afst_links = sqrt((P(loc_P,2)-obst_l(obst,2)).^2+(P(loc_P,1)-obst_l(obst,1)).^2);
 afst_rechts = sqrt((P(loc_P,2)-obst_r(obst,2)).^2+(P(loc_P,1)-obst_r(obst,1)).^2);
 
 if afst_rechts >= afst_links
     %functie  rijden
     disp('linksom')
     factor_x = -1;
     obst_choice = [obst_l(obst,1) obst_l(obst,2)];
     scatter(obst_l(obst,1), obst_l(obst,2),200)
     
 else 
     disp('rechtsom')
     factor_x = 1;
     obst_choice = [obst_r(obst,1) obst_r(obst,2)];
     scatter(obst_r(obst,1), obst_r(obst,2),200)
 end

%% doel achter de robot?
if P(loc_P,2) <= P(loc_P+5,2) %P+5 groter dus vooruit. 
    factor_achteruit = 1;
else 
    factor_achteruit = 1;
end
%% route punten opstellen. 
        if P(loc_P,1) <= P(loc_P+5,1) %als p+1 rechts van p. route buigt naar rechts op raakpunt
            factor_Px = -1;
            factor_Py = 1;
            factor_cp4 = -factor_x;
        else %route buigt naar links op raakpunt
            factor_Px = 1;
            factor_Py = 1;
            factor_cp4 = factor_x;
            %alfa_P = atan(abs((P(loc_P+1,1)-P(loc_P,1)))/abs((P(loc_P+1,2)-P(loc_P,2))));
            %cp4 = [obst_choice(1,1)+(bk/2)*cos(alfa_P) obst_choice(1,2)+(bk/2)*sin(alfa_P)];
            %dcp = [cpd*sin(alfa_P) cpd*cos(alfa_P)];
            %cp3 = [cp4(1,1)+dcp(1,1)*factor_Px cp4(1,2)-dcp(1,1)];
            %cp5 = [cp4(1,1)-dcp(1,1)*factor_Px cp4(1,2)+dcp(1,1)];
        end
% cp4 naast obstakel, cp3 terug op de route (1e curve), cp5 vooruit op de
% route(2e curve). dpc = delta centerpoints, met verschil in [x,y] tussen
% cp3,5 en cp4
cp4 = [obst_choice(1,1)+(bk/2)*cos(alfa_P)*factor_x obst_choice(1,2)+(bk/2)*sin(alfa_P)*factor_cp4];
dcp = [cpd*sin(alfa_P) cpd*cos(alfa_P)]; %delta [x y] van de cp3 en cp5 tov cp4
cp3 = [cp4(1,1)+dcp(1,1)*factor_Px*factor_achteruit cp4(1,2)-dcp(1,2)*factor_Py*factor_achteruit];
cp5 = [cp4(1,1)-dcp(1,1)*factor_Px*factor_achteruit cp4(1,2)+dcp(1,2)*factor_Py*factor_achteruit];
%% Plot de curve
        M2 =[K(1,1) K(1,2);K(1,1) (K(1,2)+cpd);cp3(1,1) cp3(1,2);cp4(1,1) cp4(1,2)];
        M3 =[cp4(1,1) cp4(1,2); cp5(1,1) cp5(1,2); M(3,1) M(3,2); M(4,1) M(4,2)];
        P = B_Curve(M2, LI);
        B_Plot(P, polyout2);
        P = B_Curve(M3, LI);
        B_Plot(P, polyout2);
else
B_Plot(P, polyout2);
end


    scatter(cp3(1,1), cp3(1,2))
    scatter(cp4(1,1), cp4(1,2))
    scatter(cp5(1,1), cp5(1,2))
    
    
    
    
    
    
    
    
    
    
    
    
    
    %% functies
function Curve = B_Curve(M, LI) 
p=M;    
n=size(M,1);
n1=n-1;
    for i=0:1:n1
    sigma(i+1)=factorial(n1)/(factorial(i)*factorial(n1-i));  % for calculating (x!/(y!(x-y)!)) values 
    end
    l=[];
    UB=[];
    maat = size(LI);
    for u=0:1/maat(1,1):1
    for d=1:n
    UB(d)=sigma(d)*((1-u)^(n-d))*(u^(d-1));
    end
    l=cat(1,l,UB);   
    end
Curve=l*p;  
end

function B_Plot(P, polyout2)
line(P(:,1),P(:,2)) %curve
plot(P(:,1),P(:,2),'r.','MarkerSize',10)
plot(polyout2) ;
end

function factor_x = Richting_kiezen(P, loc_P, obst_l, obst_r, obst)
afst_links = sqrt((P(loc_P,2)-obst_l(obst,2)).^2+(P(loc_P,1)-obst_l(obst,1)).^2);
afst_rechts = sqrt((P(loc_P,2)-obst_r(obst,2)).^2+(P(loc_P,1)-obst_r(obst,1)).^2);
 
if afst_rechts >= afst_links
     %functie  rijden
     disp('linksom')
     factor_x = -1;
     obst_choice = [obst_l(obst,1) obst_l(obst,2)];
     scatter(obst_l(obst,1), obst_l(obst,2),200)
else 
     disp('rechtsom')
     factor_x = 1;
     obst_choice = [obst_r(obst,1) obst_r(obst,2)];
     scatter(obst_r(obst,1), obst_r(obst,2),200)
end
end
    
 function rechtsom()   
 end
 
 function linksom()
 end